# Overview-about-your-grades
This Table is in german. Translations in another language are appreciated.

### The following text is in german:  
In dieser Tabelle können Schüler ihre Noten eintragen.  
Die Tabelle unterstützt sowohl mehrfach gewertete Prüfungen, sogenannte Schulaufgaben, als auch die Gewichtung einzelner Noten.  
Die Noten werden in der Tabelle Statistik auch ausgewertet.  
Diese Tabelle hat mehrere Untertabellen, dies bitte nicht vergessen.

### Download:
Am PC: Ihr könnt die Tabellen downloaden, indem ihr auf die entsprechende Datei klickt und dann Download auswählt. Oder oben rechts "Clone or Download" und dann "Download Zip" auswählt.

Am Smartphone: Geht auf "View Code". Dann auf die entsprechende Datei. Dann auf "Open binary file".

### Weitere Infos:
In der Tabelle Optionen können kleine Änderungen vorgenommen werden.  
Kommazahlen werden leider aus Platzgründen nicht angezeigt, aber als Kommazahlen mitgerechnet. Durch Veränderung der Spaltenbreite kann man sich die Kommazahlen anzeigen lassen.

Die Datei Notenbild.ods ist für LibreOffice / OpenOffice Die Datei Notenbild.xlxs ist für Excel 2007 - 2013

Über Fehler bitte informieren

Alte Versionen befinden sich im Branch "Old_Versions".
